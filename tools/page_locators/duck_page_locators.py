from selenium.webdriver.common.by import By


class DuckPageLocators:
    """
    Класс локаторов страницы с информацией об уточке.
    """
    DUCK_HEADER_LOCATORS = (By.CSS_SELECTOR, 'h1.title')
    CART_ITEMS_LOCATORS = (By.CSS_SELECTOR, '#cart .content .quantity')
    ADD_TO_CART_LOCATORS = (By.CSS_SELECTOR, '[name="add_cart_product"]')
    SELECT_LOCATORS = (By.CSS_SELECTOR, '[name="options[Size]"]')
    FULL_CART_LOCATORS = (By.CSS_SELECTOR, '[src="/litecart/includes/templates'
                                           '/default.catalog/images/cart_'
                                           'filled.png"]')
