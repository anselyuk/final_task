from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс локаторов главной страницы.
    """
    EMAIL_LOCATORS = (By.CSS_SELECTOR, '[name="email"]')
    PASSWORD_LOCATORS = (By.CSS_SELECTOR, '[name="password"]')
    LOGIN_BUTTON_LOCATORS = (By.CSS_SELECTOR, '[name="login"]')
    DUCK_INFO_LOCATORS = (By.CSS_SELECTOR, '[src="/litecart/cache/'
                                           '9e29b832ba49f5808f75748a1a385cb004'
                                           '8d5afc160x160_fwb.png"]')
