from selenium.webdriver.common.by import By


class LoginPageLocators:
    """
    Класс локаторов главной страницы с правами авторизованного пользователя.
    """
    SUCCESS_NOTICE_LOCATORS = (By.CSS_SELECTOR, '.notice.success')
    ACCOUNT_LOCATORS = (By.XPATH, '//a[text()="Edit Account"]')
    LOGOUT_LOCATORS = (By.XPATH, '//a[text()="Logout"]')
