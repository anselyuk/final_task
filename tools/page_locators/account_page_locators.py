from selenium.webdriver.common.by import By


class AccountPageLocators:
    """
    Класс локаторов страницы редактирования аккаунта пользователя.
    """
    HEADER_LOCATORS = (By.CSS_SELECTOR, '.title')
    USERNAME_LOCATORS = (By.CSS_SELECTOR, '[name="firstname"]')
    USERNAME_LOCATORS_STRING = (By.CSS_SELECTOR, 'div')
    SAVE_LOCATORS = (By.CSS_SELECTOR, '[name="save"]')
    LOGOUT_LOCATORS = (By.XPATH, '//a[text()="Logout"]')
