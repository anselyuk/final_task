from selenium.webdriver.common.by import By


class CartPageLocators:
    """
    Класс локаторов страницы корзины.
    """
    DUCK_PCS_LOCATORS = (By.CSS_SELECTOR, '[name="quantity"]')
    UPDATE_LOCATORS = (By.CSS_SELECTOR, '[name="update_cart_item"]')
    TOTAL_SUMMARY_LOCATORS = (By.CSS_SELECTOR, '.footer :nth-child(2)>strong')
    REMOVE_LOCATORS = (By.CSS_SELECTOR, '[name="remove_cart_item"]')
    NO_ITEMS_LOCATORS = (By.CSS_SELECTOR, '#checkout-cart-wrapper em')
