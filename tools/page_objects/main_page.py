from tools.page_objects.base_page import BasePage
from tools.page_objects.login_page import LoginPage
from tools.page_objects.duck_page import DuckPage
from tools.page_locators.main_page_locators import MainPageLocators


class MainPage(BasePage):
    """
    Класс главной страницы сайта.
    """
    URL = "http://127.0.0.1/litecart/en/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def email_input(self):
        """
        Ввод email в форму Login.
        :return: введенный email в форму Login
        """
        email_address = self.find_element(MainPageLocators.EMAIL_LOCATORS)
        email_address.send_keys("anselyuk@gmail.com")

    def password_input(self):
        """
        Ввод пароля в форму Login.
        :return: введенный пароль в форму Login
        """
        password = self.find_element(MainPageLocators.PASSWORD_LOCATORS)
        password.send_keys("123456")

    def login(self):
        """
        Авторизация пользователя на сайте.
        :return: главная страница с правами авторизованного пользователя
        """
        login_but = self.find_element(MainPageLocators.LOGIN_BUTTON_LOCATORS)
        login_but.click()
        return LoginPage(self.driver, self.driver.current_url)

    def duck_info(self):
        """
        Переход на страницу с информацией о желтой уточке.
        :return: страница с информацией о желтой уточке
        """
        duck_but = self.find_element(MainPageLocators.DUCK_INFO_LOCATORS)
        duck_but.click()
        return DuckPage(self.driver, self.driver.current_url)
