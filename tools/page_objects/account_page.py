from tools.page_objects.base_page import BasePage
from tools.page_locators.account_page_locators import AccountPageLocators


class AccountPage(BasePage):
    """
    Класс страницы редактирования аккаунта пользователя.
    """

    def get_account_header(self):
        """
        Получение заголовка аккаунта пользователя.
        :return: заголовок акккаунта пользователя
        """
        return self.find_element(AccountPageLocators.HEADER_LOCATORS)

    def get_username(self):
        """
        Получение имени пользвателя.
        :return: имя пользователя
        """
        return self.find_element(AccountPageLocators.USERNAME_LOCATORS)

    def save_changes(self):
        """
        Нажатие на кнопку для сохранения изменений.
        """
        save_button = self.find_element(AccountPageLocators.SAVE_LOCATORS)
        save_button.click()

    @staticmethod
    def del_username(username):
        """
        Удаление имени пользователя.
        :param username: имя пользователя
        """
        username.clear()

    @staticmethod
    def change_name(username, name):
        """
        Изменение имени пользователя на Changed Username.
        :param name: новое имя пользователя
        :param username: имя пользователя
        """
        username.send_keys(name)

    def logout(self):
        """
        Разлогиниться в приложении.
        """
        logout_account = self.find_element(AccountPageLocators.LOGOUT_LOCATORS)
        logout_account.click()
