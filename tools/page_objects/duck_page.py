from tools.page_objects.base_page import BasePage
from tools.page_objects.cart_page import CartPage
from tools.page_locators.duck_page_locators import DuckPageLocators
from selenium.webdriver.support.select import Select


class DuckPage(BasePage):
    """
    Класс страницы c уточкой.
    """

    def duck_header(self):
        """
        Заголовок страницы с информацией об уточке.
        :return: заголовок страницы с информацией об уточке
        """
        return self.find_element(DuckPageLocators.DUCK_HEADER_LOCATORS)

    def select_duck_size(self):
        """
        Выбор размера уточки Small.
        """
        select_sz = Select(self.find_element(DuckPageLocators.SELECT_LOCATORS))
        select_sz.select_by_value('Small')

    def add_duck_to_cart(self):
        """
        Добавление уточки в корзину.
        """
        add_duck_but = self.find_element(DuckPageLocators.ADD_TO_CART_LOCATORS)
        add_duck_but.click()

    def full_cart_image(self):
        """
        Поиск изображения с заполненной корзиной.
        :return: изображение с заполненной корзиной
        """
        return self.find_element(DuckPageLocators.FULL_CART_LOCATORS)

    def cart_items_after_duck_addition(self):
        """
        Получение количества товаров в корзине.
        :return: количество товаров в корзине
        """
        return self.find_element(DuckPageLocators.CART_ITEMS_LOCATORS)

    def open_cart_page(self):
        """
        Открытие корзины путем нажатия на изображение заполненной корзины.
        :return: страница корзины
        """
        cart_img = self.find_element(DuckPageLocators.FULL_CART_LOCATORS)
        cart_img.click()
        return CartPage(self.driver, self.driver.current_url)
