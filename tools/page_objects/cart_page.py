from tools.page_objects.base_page import BasePage
from tools.page_locators.cart_page_locators import CartPageLocators


class CartPage(BasePage):
    """
    Класс страницы корзины.
    """

    def get_duck_pcs(self):
        """
        Поиск поля с количеством уточек в корзине.
        :return: поле ввода количества уточек в корзине
        """
        return self.find_element(CartPageLocators.DUCK_PCS_LOCATORS)

    def update_order(self):
        """
        Обновление заказа путем нажатия на кнопку Update.
        """
        update_button = self.find_element(CartPageLocators.UPDATE_LOCATORS)
        update_button.click()

    def get_total_summary(self):
        """
        Получение итоговой суммы заказа.
        :return: итоговая сумма заказа
        """
        return self.find_element(CartPageLocators.TOTAL_SUMMARY_LOCATORS)

    def remove_ducks(self):
        """
        Удаление уточек из корзины путем нажатия на кнопку Remove.
        """
        remove_button = self.find_element(CartPageLocators.REMOVE_LOCATORS)
        remove_button.click()

    def no_items_in_cart(self):
        """
        Поиск сообщения о пустой корзине.
        :return: сообщение о пустой корзине
        """
        return self.find_element(CartPageLocators.NO_ITEMS_LOCATORS)

    @staticmethod
    def del_duck_pcs(pcs):
        """
        Удаление количества уточке в корзине.
        :param pcs: поле ввода количества уточек в корзине
        """
        pcs.clear()

    @staticmethod
    def change_duck_pcs_to_3(pcs):
        """
        Изменение количества уточек в корзине на 3.
        :param pcs: поле ввода количества уточек
        """
        pcs.send_keys("3")
