from tools.page_objects.base_page import BasePage
from tools.page_objects.account_page import AccountPage
from tools.page_locators.login_page_locators import LoginPageLocators


class LoginPage(BasePage):
    """
    Класс главной страницы с правами авторизованного пользователя.
    """

    def get_success_notice(self):
        """
        Получение записи об успешной авторизации в приложении.
        :return: запись об успешной авторизации
        """
        return self.find_element(LoginPageLocators.SUCCESS_NOTICE_LOCATORS)

    def open_edit_account(self):
        """
        Открытие окна редактирования личной информации авторизованного
        пользователя.
        :return: страница редактирования личной информации пользователя
        """
        edit_account = self.find_element(LoginPageLocators.ACCOUNT_LOCATORS)
        edit_account.click()
        return AccountPage(self.driver, self.driver.current_url)

    def logout(self):
        """
        Разлогиниться в приложении.
        """
        logout_account = self.find_element(LoginPageLocators.LOGOUT_LOCATORS)
        logout_account.click()
