import mysql.connector as mysql


class LitecartDB:
    """
    Класс БД Litecart.
    """

    @staticmethod
    def connect_db():
        """
        Подключение к БД Litecart.
        :return: БД и объект cursor для взаимодействия с ней
        """
        db = mysql.connect(
            host="localhost",
            user="root",
            passwd="",
            database='litecart'
        )

        cursor = db.cursor()
        return db, cursor

    @staticmethod
    def close_db(db):
        """
        Закрытие БД.
        :param db: БД
        """
        db.close()

    @staticmethod
    def select_username_2(cursor):
        """
        Имя пользвателя в бд.
        :param cursor: объект для взаимодействия с БД
        """
        cursor.execute("SELECT firstname FROM lc_customers "
                       "WHERE id = 2")
        return cursor.fetchall()

    @staticmethod
    def check_db_username():
        """
        Подключение к БД для получения имени пользователя с id = 2.
        :return: имя пользователя с id = 2
        """
        db, cursor = LitecartDB.connect_db()
        username = LitecartDB.select_username_2(cursor)
        LitecartDB.close_db(db)
        return username
