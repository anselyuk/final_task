import allure

from tools.page_objects.main_page import MainPage


@allure.feature("Login page")
@allure.story("Authorization")
def test_success_login_notice(driver):
    """
    Проверка, что отображается запись об успешной авторизации.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    main_page.email_input()
    main_page.password_input()
    login_page = main_page.login()
    success_notice = "You are now logged in as Nastassia Kolosey."
    with allure.step("Check that the success notice is displayed after "
                     "successful authorization."):
        assert login_page.get_success_notice().text == success_notice, \
            "Success authorization notice isn't displayed!"
    login_page.logout()
