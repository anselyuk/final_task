import allure

from tools.page_objects.main_page import MainPage


@allure.feature("Cart page")
@allure.story("Order")
def test_check_price_for_3_ducks(driver):
    """
    Проверка того, что после изменения количества уточек в корзине на 3,
    стоимость заказа станет в 3 раза больше.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    duck_page = main_page.duck_info()
    duck_page.select_duck_size()
    duck_page.add_duck_to_cart()
    cart_page = duck_page.open_cart_page()
    duck_pcs = cart_page.get_duck_pcs()
    cart_page.del_duck_pcs(duck_pcs)
    cart_page.change_duck_pcs_to_3(duck_pcs)
    with allure.step("Check that the cart contains 3 ducks'."):
        assert duck_pcs.get_attribute('value') == "3", \
            "Not 3 ducks in the cart!"
    cart_page.update_order()
    driver.refresh()
    with allure.step("Check that the total order summary is $54.00'."):
        assert cart_page.get_total_summary().text == "$54.00", \
            "Order total summary is incorrect!"


@allure.feature("Cart page")
@allure.story("Remove button")
def test_remove_ducks(driver):
    """
    Проверка того, что корзина пустая после удаления уточек.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    duck_page = main_page.duck_info()
    duck_page.select_duck_size()
    duck_page.add_duck_to_cart()
    cart_page = duck_page.open_cart_page()
    cart_page.remove_ducks()
    no_items_label = cart_page.no_items_in_cart().text
    with allure.step("Check that the cart is empty after clicking on "
                     "'Remove' button'."):
        assert no_items_label == "There are no items in your cart.", \
            "Cart isn't empty after removing items!"
