# -*- coding: utf-8 -*-
import pytest
import requests
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    """
    Запуск и закрытие драйвера Chrome.
    :return: экземпляр класса драйвера Chrome
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(options=chrome_options,
                              executable_path="C:\driver\chromedriver.exe")
    driver.maximize_window()
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def post_new_pet():
    """
    Добавление нового питомца.
    :return: код ответа на запрос
    """
    new_pet = {"id": 666, "category": {"id": 0, "name": "string"},
               "name": "doggie", "photoUrls": ["string"],
               "tags": [{"id": 0, "name": "string"}],
               "status": "available"}
    address = 'https://petstore.swagger.io/v2/pet'
    response = requests.post(address, json=new_pet)
    return response.status_code


@pytest.fixture(scope='session')
def put_pet():
    """
    Обновление имени питомца.
    :return: код ответа на запрос
    """
    updated_pet = {"id": 1, "category": {"id": 0, "name": "string"},
                   "name": "Loky", "photoUrls": ["string"],
                   "tags": [{"id": 0, "name": "string"}],
                   "status": "available"}
    address = 'https://petstore.swagger.io/v2/pet'
    response = requests.put(address, json=updated_pet)
    return response.status_code


@pytest.fixture(scope='session')
def post_user():
    """
    Создание пользователя.
    :return: код ответа на запрос
    """
    new_user = [{"id": 999, "username": "nastassia", "firstName": "string",
                 "lastName": "string", "email": "string",
                 "password": "string", "phone": "string",
                 "userStatus": 0}]
    address = 'https://petstore.swagger.io/v2/user/createWithList'
    response = requests.post(address, json=new_user)
    return response.status_code


@pytest.fixture(scope='session')
def get_user():
    """
    Получение данных пользователя.
    :return: код ответа на запрос
    """
    address = 'https://petstore.swagger.io/v2/user/nastassia'
    response = requests.get(address)
    return response.status_code


@pytest.fixture(scope='session')
def put_user():
    """
    Обновление username пользователя.
    :return: код ответа на запрос
    """
    updated_user = {"id": 999, "username": "Nastassia",
                    "firstName": "string", "lastName": "string",
                    "email": "string", "password": "string",
                    "phone": "string", "userStatus": 0}
    address = 'https://petstore.swagger.io/v2/user/nastassia'
    response = requests.put(address, json=updated_user)
    return response.status_code
