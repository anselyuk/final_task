import allure

from tools.page_objects.main_page import MainPage
from tools.db.litecart_db import LitecartDB


@allure.feature("Account page")
@allure.story("Account header")
def test_account_header(driver):
    """
    Проверка, что в заголовке страницы отображается 'Edit Account'.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    main_page.email_input()
    main_page.password_input()
    login_page = main_page.login()
    account_page = login_page.open_edit_account()
    with allure.step("Check that account header is 'Edit Account'."):
        assert account_page.get_account_header().text == "Edit Account", \
            "Incorrect account header!"
    login_page.logout()


@allure.feature("Account page")
@allure.story("Username")
def test_changed_username_is_saved(driver):
    """
    Проверка, что после нажатия кнопки Save измененное имя отображается на
    странице и такое же имя сохранено в БД.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    main_page.email_input()
    main_page.password_input()
    login_page = main_page.login()
    account_page = login_page.open_edit_account()
    username = account_page.get_username()
    account_page.del_username(username)
    account_page.change_name(username, "Changed Username")
    account_page.save_changes()
    username = account_page.get_username()
    ui_username = username.get_attribute('value')
    username_db = str(LitecartDB.check_db_username())
    with allure.step("Check that changed username is saved in the db.'"):
        assert ui_username == "Changed Username" == username_db[3:-4], \
            "Username isn't changed!"
    account_page.del_username(username)
    account_page.change_name(username, "Nastassia")
    account_page.save_changes()
    account_page.logout()
