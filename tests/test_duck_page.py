import allure

from tools.page_objects.main_page import MainPage


@allure.feature("Duck info page")
@allure.story("Add button")
def test_add_duck_to_cart(driver):
    """
    Проверка добавления уточки в корзину на странице с информацией об уточке.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    duck_page = main_page.duck_info()
    with allure.step("Check that duck info page is open after clicking on duck"
                     " image."):
        assert "Duck" in duck_page.duck_header().text, \
            "Duck info page isn't open!"
    duck_page.select_duck_size()
    duck_page.add_duck_to_cart()
    duck_page.full_cart_image()
    cart_items = duck_page.cart_items_after_duck_addition()
    with allure.step("Check that duck is added to the cart after clicking on "
                     "'Add to Cart' button."):
        assert cart_items.text == "1", "Duck isn't added to the cart!"
