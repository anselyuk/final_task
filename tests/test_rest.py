import allure


@allure.feature("REST requests")
@allure.story("POST request")
def test_new_pet_is_added(post_new_pet):
    """
    Проверка того, что новый питомец добавлен успешно.
    """
    with allure.step("Check that the new pet is added successfully"):
        print(post_new_pet)
        assert 200 == post_new_pet, "New pet isn't added!"


@allure.feature("REST requests")
@allure.story("PUT request")
def test_pet_is_updated(put_pet):
    """
    Проверка того, что имя питомца обновлено успешно.
    """
    with allure.step("Check that the pet info is updated."):
        assert 200 == put_pet, "Pet info isn't updated!"


@allure.feature("REST requests")
@allure.story("POST request")
def test_new_user_is_created(post_user):
    """
    Проверка того, что новый пользователь был добавлен успешно.
    """
    with allure.step("Check that the new user is created."):
        assert 200 == post_user, "New user isn't created!"


@allure.feature("REST requests")
@allure.story("GET request")
def test_new_userinfo_is_got(get_user):
    """
    Проверка того, что информация о новом пользователе получена успешно.
    """
    with allure.step("Check that the userinfo is got successfully."):
        assert 200 == get_user, "Userinfo isn't got!"


@allure.feature("REST requests")
@allure.story("PUT request")
def test_user_is_updated(put_user):
    """
    Проверка того, что данные о пользователе обновлены успешно.
    """
    with allure.step("Check that the userinfo is updated successfully."):
        assert 200 == put_user, "Userinfo isn't updated!"
